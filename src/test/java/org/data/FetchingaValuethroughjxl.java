package org.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class FetchingaValuethroughjxl {

	public static void main(String[] args) throws BiffException, IOException {
		File f=new File("/home/thirusha/Documents/Testdata/TestData2.xlsx");
		FileInputStream fis=new FileInputStream(f);
		Workbook book= Workbook.getWorkbook(fis);
		Sheet sh=book.getSheet("Sheet1");
		String celldata=sh.getCell(1,2).getContents();
		System.out.println("value present is "+celldata);
		
		int rows = sh.getRows();
		int columns= sh.getColumns();
		
		for(int i=0; i<rows;i++) {
			for(int j=0; j<columns; j++) {
				celldata=sh.getCell(j,i).getContents();
				System.out.println(celldata);
			}
		}
	}

}
